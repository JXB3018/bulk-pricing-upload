.PHONY: run build-lm build-win

run:
	go run cmd/bulk-pricing-upload/main.go --ENV=STG

build-lm:
	go build -o ./dist/bulk-pricing-upload ./cmd/bulk-pricing-upload/main.go

build-win:
	GOOS=windows GOARCH=amd64 go build -o ./dist/bulk-pricing-upload.exe ./cmd/bulk-pricing-upload/main.go
