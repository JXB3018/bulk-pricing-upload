package menu

import (
	"bulk-pricing-upload/internal/models"
	"bulk-pricing-upload/internal/queries"
	"encoding/json"
)

// FetchAllGlobalProducts fetches all global products by paginating through the results.
func (c *client) FetchAllGlobalProducts() ([]models.Node, error) {
	var allProducts []models.Node // Slice to hold all products
	after := ""                   // Pagination cursor

	// Loop to paginate through all products
	for {
		// Fetch a page of global products
		response, err := c.fetchGlobalProducts(after)
		if err != nil {
			return nil, err
		}

		// Append the fetched products to the allProducts slice
		for _, edge := range response.Data.ProductDefinitions.Edges {
			allProducts = append(allProducts, edge.Node)
			after = edge.Cursor // Update the cursor for the next page
		}

		// Break the loop if there are no more pages
		if !response.Data.ProductDefinitions.PageInfo.HasNextPage {
			break
		}
	}

	return allProducts, nil
}

// fetchGlobalProducts fetches a single page of global products using the provided cursor.
func (c *client) fetchGlobalProducts(after string) (*models.Response, error) {
	// Define variables for the GraphQL query
	variables := map[string]interface{}{
		"after": after,
	}

	// Execute the GraphQL query to fetch products
	body, err := c.graphQLClient.ExecuteGraphQLQuery(queries.ListProducts, variables)
	if err != nil {
		return nil, err
	}

	// Unmarshal the response body into the Response struct
	var response models.Response
	err = json.Unmarshal(body, &response)
	if err != nil {
		return nil, err
	}

	return &response, nil
}
