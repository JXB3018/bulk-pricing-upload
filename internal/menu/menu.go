package menu

import (
	"bulk-pricing-upload/internal/graphql"
	"bulk-pricing-upload/internal/models"
	"log"
)

// Client interface defines the method to fetch all global products.
type Client interface {
	FetchAllGlobalProducts() ([]models.Node, error)
	GetCompressedMenu(storeNumber string) (string, error)
	PublishMenu(storeNumbers []string) (string, error)
}

// client struct holds the GraphQL client.
type client struct {
	graphQLClient graphql.Client
}

// NewClient initializes and returns a new menu client instance.
func NewClient(graphQLClient graphql.Client) Client {
	return &client{graphQLClient: graphQLClient}
}

// GetCompressedMenu fetches the compressed menu for a store.
func (c *client) GetCompressedMenu(storeNumber string) (string, error) {
	// This is a stub function. Implementation will be added later.

	// TODO: Add logic to fetch the compressed menu for the given store number.
	// -- url = stores.CompressedMenuByChannel
	// -- http GET with compressed url

	log.Println("GetCompressedMenu is not yet implemented")
	return "GetCompressedMenu is not yet implemented", nil
}

// PublishMenu publishes the menu for the given store numbers.
func (c *client) PublishMenu(storeNumbers []string) (string, error) {
	// This is a stub function. Implementation will be added later.

	// TODO: Add logic to publish the menu for the given store numbers.

	log.Println("PublishMenu is not yet implemented")
	return "PublishMenu is not yet implemented", nil
}
