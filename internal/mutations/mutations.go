package mutations

const (
	PublishMenuDefinitionsAsync = `
	mutation PublishMenuDefinitionsAsync($input:    PublishMenuDefinitionsAsyncInput!) {
		publishMenuDefinitionsAsync(input: $input) {
			jobId
		}
  	}`
)
