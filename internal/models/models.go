package models

// GraphQLQuery represents a GraphQL query with its variables.
type GraphQLQuery struct {
	Query     string                 `json:"query"`
	Variables map[string]interface{} `json:"variables"`
}

// Response represents the response structure for product definitions.
type Response struct {
	Data struct {
		ProductDefinitions ProductDefinitions `json:"productDefinitions"`
	} `json:"data"`
}

// PatchResponse represents the response structure for patches.
type PatchResponse struct {
	Data struct {
		MenuPatches struct {
			Edges []struct {
				Node struct {
					PatchID string `json:"patchId"`
					Name    string `json:"name"`
				} `json:"node"`
			} `json:"edges"`
		} `json:"menuPatches"`
	} `json:"data"`
}

// ProductDefinitions represents a list of product definitions with pagination info.
type ProductDefinitions struct {
	Edges    []Edge   `json:"edges"`
	PageInfo PageInfo `json:"pageInfo"`
}

// Edge represents a single edge in the product definitions list.
type Edge struct {
	Cursor string `json:"cursor"`
	Node   Node   `json:"node"`
}

// Node represents a single product definition node.
type Node struct {
	ProductID    string    `json:"productId"`
	Name         string    `json:"name"`
	InternalName string    `json:"internalName"`
	ProductCode  string    `json:"productCode"`
	BrandTags    []string  `json:"brandTags"`
	PromoTags    []string  `json:"promoTags"`
	Variants     []Variant `json:"variants"`
}

// Variant represents a single variant of a product.
type Variant struct {
	VariantID                string           `json:"variantId"`
	Modifiable               bool             `json:"modifiable"`
	MaxAllowedModifierWeight interface{}      `json:"maxAllowedModifierWeight"`
	BrandTags                []string         `json:"brandTags"`
	PromoTags                []string         `json:"promoTags"`
	Name                     interface{}      `json:"name"`
	VariantCode              string           `json:"variantCode"`
	Price                    Money            `json:"price"`
	AuthoringPrice           AuthoringPrice   `json:"authoringPrice"`
	SelectedOptions          []SelectedOption `json:"selectedOptions"`
}

// Money represents the price of a variant in a specific currency.
type Money struct {
	Amount       int    `json:"amount"`
	CurrencyCode string `json:"currencyCode"`
}

// AuthoringPrice represents the authoring price of a variant.
type AuthoringPrice struct {
	Amount     int         `json:"amount"`
	NamedPrice interface{} `json:"namedPrice"`
}

// SelectedOption represents a selected option for a variant.
type SelectedOption struct {
	OptionType  OptionType  `json:"optionType"`
	OptionValue OptionValue `json:"optionValue"`
}

// OptionType represents the type of an option.
type OptionType struct {
	OptionTypeID   string `json:"optionTypeId"`
	OptionTypeCode string `json:"optionTypeCode"`
	InternalName   string `json:"internalName"`
}

// OptionValue represents the value of an option.
type OptionValue struct {
	OptionValueID   string `json:"optionValueId"`
	OptionValueCode string `json:"optionValueCode"`
	InternalName    string `json:"internalName"`
}

// PageInfo represents pagination information.
type PageInfo struct {
	HasNextPage     bool `json:"hasNextPage"`
	HasPreviousPage bool `json:"hasPreviousPage"`
}

// Price represents a price with its amount and currency code.
type Price struct {
	Amount       int    `json:"amount"`
	CurrencyCode string `json:"currencyCode"`
}

// SetVariantPrice represents an operation to set the price of a variant.
type SetVariantPrice struct {
	VariantID string `json:"variantId"`
	Price     Price  `json:"price"`
}

// Operation represents an operation to be performed on a variant.
type Operation struct {
	SetVariantPrice SetVariantPrice `json:"setVariantPrice"`
}

// StoreOperations represents a list of operations for a store.
type StoreOperations struct {
	Name          string      `json:"name"`
	PatchID       string      `json:"patchId"`
	SetOperations []Operation `json:"setOperations"`
}

// StoreResponse represents the response structure for a store.
type StoreResponse struct {
	StoreNumber             string           `json:"storeNumber"`
	AssignedMenuDefinitions []MenuDefinition `json:"assignedMenuDefinitions"`
	CompressedMenuByChannel struct {
		URL    string `json:"url"`
		Expiry string `json:"expiry"`
	} `json:"compressedMenuByChannel"`
}

// MenuDefinition represents a menu definition assigned to a store.
type MenuDefinition struct {
	MenuID  string          `json:"menuId"`
	Name    string          `json:"name"`
	Patches []PatchResponse `json:"patches"`
}

// StoreEdge represents a single edge in the store list.
type StoreEdge struct {
	Cursor string        `json:"cursor"`
	Node   StoreResponse `json:"node"`
}

// StorePageInfo represents pagination information for stores.
type StorePageInfo struct {
	HasNextPage     bool `json:"hasNextPage"`
	HasPreviousPage bool `json:"hasPreviousPage"`
}

// StoreList represents a list of stores with pagination info.
type StoreList struct {
	Edges    []StoreEdge   `json:"edges"`
	PageInfo StorePageInfo `json:"pageInfo"`
}

// StoreQueryResponse represents the response structure for a store query.
type StoreQueryResponse struct {
	Data struct {
		Stores StoreList `json:"stores"`
	} `json:"data"`
}
