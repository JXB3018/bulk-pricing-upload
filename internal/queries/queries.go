package queries

const (
	// MENU
	ListProducts = `
	query ListProducts($after: String) {
		productDefinitions (first:100, orderBy: {internalName: ASC}, after: $after) {
		  edges {
			cursor 
			node {
			  ...ProductResponse
			}
		  }
		  pageInfo {
			hasNextPage
			hasPreviousPage
		  }
		}
	}
	  
	fragment ProductResponse on ProductDefinition {
		productId
		name
		internalName
		productCode
		brandTags
		promoTags
		variants {
			variantId
			modifiable
			maxAllowedModifierWeight
			brandTags
			promoTags
			name
			variantCode
			price {
			...MoneyFragment
			}
			authoringPrice {
			...AuthoringPriceFragment
			}
			selectedOptions {
			optionType {
				optionTypeId
				optionTypeCode
				internalName
			}
			optionValue {
				optionValueId
				optionValueCode
				internalName
			}
			}
		}
	}
	
	fragment MoneyFragment on Money {
		amount
		currencyCode
	}
	
	fragment AuthoringPriceFragment on AuthoringPrice {
		amount
		namedPrice {
			...NamedPriceDefinitionFragment
		}
	}

	fragment NamedPriceDefinitionFragment on NamedPriceDefinition {
		name
		amount
	}`

	PublishMenuDefinitionsStatus = `
	query PublishMenuDefinitionsStatus($jobId: UUID!) {
		publishMenuDefinitionsAsyncStatus(jobId: $jobId) {
			status
		}
	}`

	// PATCHES
	ListPatches = `
	query ListPatches($name: String) {
		menuPatches(first: 100, orderBy: {name: ASC}, name: $name) {
		  edges {
			node {
			  ...PatchResponse
			}
		  }
		}
	  }
	  
	fragment PatchResponse on MenuPatch {
		patchId
		name
	}`

	// STORES
	ListStores = `
	query ListStores($after: String) {
		stores(after: $after) {
			edges {
				cursor
				node {
					franchiseCode
					storeNumber
					assignedMenuDefinitions: menuChannels(name: ECOMM) {
						menuDefinition: assignedMenuDefinition {
							menuId
							name
						}
						patches: assignedPatches {
							... PatchResponse
						}
					}
					compressedMenuByChannel(channel: WEB) {
						url
						expiry
					}
				}
			}
			pageInfo {
				hasNextPage
				hasPreviousPage
			}
		}
	}
		
	fragment PatchResponse on MenuPatch {
		patchId
		name
	}`
)
