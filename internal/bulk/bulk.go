package bulk

import (
	"bulk-pricing-upload/internal/models"
	"encoding/csv"
	"log"
	"os"
	"strconv"
	"strings"
)

// ReadCSVAndCreateOperations reads a CSV file from the provided file path,
// and creates a map of store operations grouped by store ID.
func ReadCSVAndCreateOperations(filePath string) (map[string][]models.Operation, error) {
	// Open the CSV file
	file, err := os.Open(filePath)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	// Create a new CSV reader
	reader := csv.NewReader(file)
	records, err := reader.ReadAll()
	if err != nil {
		return nil, err
	}

	// Initialize a map to hold operations grouped by store
	storeOperations := make(map[string][]models.Operation)

	// Iterate through the records, skipping the header row
	for i, record := range records {
		if i == 0 {
			continue
		}

		// Ensure the record has at least 3 fields
		if len(record) < 3 {
			log.Println("Skipping malformed record (less than 3 fields):", record)
			continue
		}

		// Extract and clean the variant ID
		variantID := strings.Trim(record[1], "\uFEFF") // Remove BOM if present

		// Convert the price amount from string to integer
		priceAmount, err := strconv.Atoi(record[2])
		if err != nil {
			log.Println("Skipping record with invalid price:", record)
			continue
		}

		// Extract the store ID
		store := record[0]

		// Create an operation for the variant price update
		operation := models.Operation{
			SetVariantPrice: models.SetVariantPrice{
				VariantID: variantID,
				Price: models.Price{
					Amount:       priceAmount,
					CurrencyCode: "USD",
				},
			},
		}

		// Append the operation to the store's operations list
		storeOperations[store] = append(storeOperations[store], operation)
	}

	// Return the map of store operations
	return storeOperations, nil
}
