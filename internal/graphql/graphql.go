package graphql

import (
	"bulk-pricing-upload/internal/auth"
	"bulk-pricing-upload/internal/models"
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
)

// Client interface defines the method to execute a GraphQL query.
type Client interface {
	ExecuteGraphQLQuery(query string, variables map[string]interface{}) ([]byte, error)
}

// client struct holds the authentication client and base URL.
type client struct {
	authClient auth.Client
	baseURL    string
}

// NewClient initializes and returns a new GraphQL client instance.
func NewClient(authClient auth.Client, baseURL string) Client {
	return &client{authClient: authClient, baseURL: baseURL}
}

// ExecuteGraphQLQuery executes a GraphQL query with the provided query string and variables.
func (c *client) ExecuteGraphQLQuery(query string, variables map[string]interface{}) ([]byte, error) {
	// Marshal the query and variables into a JSON request body.
	requestBody, err := json.Marshal(models.GraphQLQuery{Query: query, Variables: variables})
	if err != nil {
		return nil, err
	}

	// Create a new HTTP POST request with the request body.
	req, err := http.NewRequest("POST", c.baseURL, bytes.NewBuffer(requestBody))
	if err != nil {
		return nil, err
	}

	// Set the authorization header using the authentication client.
	c.authClient.SetAuthorizationHeader(req)

	// Create an HTTP client and send the request.
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	// Read the response body.
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	// Return the response body.
	return body, nil
}
