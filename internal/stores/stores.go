package stores

import (
	"bulk-pricing-upload/internal/graphql"
	"bulk-pricing-upload/internal/models"
	"bulk-pricing-upload/internal/queries"
	"encoding/json"
)

// Client interface defines the method to fetch all stores.
type Client interface {
	FetchAllStores() ([]models.StoreResponse, error)
}

// client struct holds the GraphQL client for making requests.
type client struct {
	graphQLClient graphql.Client
}

// NewClient initializes and returns a new stores client instance.
func NewClient(graphQLClient graphql.Client) Client {
	return &client{graphQLClient: graphQLClient}
}

// FetchAllStores fetches all stores by paginating through the results.
func (c *client) FetchAllStores() ([]models.StoreResponse, error) {
	var allStores []models.StoreResponse // Slice to hold all stores
	after := ""                          // Pagination cursor

	// Loop to paginate through all stores
	for {
		// Fetch a page of stores
		response, err := c.fetchStores(after)
		if err != nil {
			return nil, err
		}

		// Append the fetched stores to the allStores slice
		for _, edge := range response.Data.Stores.Edges {
			allStores = append(allStores, edge.Node)
			after = edge.Cursor // Update the cursor for the next page
		}

		// Break the loop if there are no more pages
		if !response.Data.Stores.PageInfo.HasNextPage {
			break
		}
	}

	return allStores, nil
}

// fetchStores fetches a single page of stores using the provided cursor.
func (c *client) fetchStores(after string) (*models.StoreQueryResponse, error) {
	// Define variables for the GraphQL query
	variables := map[string]interface{}{
		"after": after,
	}

	// Execute the GraphQL query to fetch stores
	body, err := c.graphQLClient.ExecuteGraphQLQuery(queries.ListStores, variables)
	if err != nil {
		return nil, err
	}

	// Unmarshal the response body into the StoreQueryResponse struct
	var response models.StoreQueryResponse
	if err := json.Unmarshal(body, &response); err != nil {
		return nil, err
	}

	return &response, nil
}
