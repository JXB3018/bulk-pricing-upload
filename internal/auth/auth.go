package auth

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"time"
)

// Client interface defines the methods for authorization and setting authorization headers.
type Client interface {
	Authorize() error
	SetAuthorizationHeader(req *http.Request) *http.Request
}

// client struct holds the client credentials, auth URL, and token.
type client struct {
	clientCredentials string
	authURL           string
	token             AuthResponse
}

// NewClient initializes and returns a new Client instance.
func NewClient(clientCredentials, authURL string) Client {
	return &client{
		clientCredentials: clientCredentials,
		authURL:           authURL,
	}
}

// AuthResponse represents the structure of the authorization response.
type AuthResponse struct {
	AccessToken string `json:"access_token"`
	ExpiresIn   int64  `json:"expires_in"`
}

// Authorize authenticates the client and obtains an access token.
func (c *client) Authorize() error {
	// Construct the request body with the grant type.
	data := url.Values{}
	data.Set("grant_type", "client_credentials")

	// Create a new HTTP POST request to the authorization URL.
	req, err := http.NewRequest("POST", c.authURL, bytes.NewBufferString(data.Encode()))
	if err != nil {
		return fmt.Errorf("failed to create request: %w", err)
	}

	// Set the necessary headers for the request.
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Set("Authorization", fmt.Sprintf("Basic %s", base64.StdEncoding.EncodeToString([]byte(c.clientCredentials))))

	// Create an HTTP client and send the request.
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return fmt.Errorf("failed to send request: %w", err)
	}
	defer resp.Body.Close()

	// Check if the response status code is OK (200).
	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("request failed with status code %d", resp.StatusCode)
	}

	// Parse the response JSON to get the access token and expiration time.
	var authResponse AuthResponse
	if err := json.NewDecoder(resp.Body).Decode(&authResponse); err != nil {
		return fmt.Errorf("failed to decode response: %w", err)
	}

	// Get the current epoch time (Unix timestamp) and add an hour to the expiration time.
	currentTime := time.Now().UTC().Unix()
	authResponse.ExpiresIn = currentTime + int64(time.Hour.Seconds())

	// Store the token in the client struct.
	c.token = authResponse
	return nil
}

// SetAuthorizationHeader sets the authorization header for a given HTTP request.
func (c *client) SetAuthorizationHeader(req *http.Request) *http.Request {
	currentTime := time.Now().UTC().Unix()
	// Reauthorize if the token has expired.
	if c.token.ExpiresIn < currentTime {
		if err := c.Authorize(); err != nil {
			fmt.Println("failed to reauthorize:", err)
			return req
		}
	}
	// Set the Content-Type and Authorization headers.
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %v", c.token.AccessToken))
	return req
}
