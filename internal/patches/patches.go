package patches

import (
	"bulk-pricing-upload/internal/graphql"
	"bulk-pricing-upload/internal/models"
	"bulk-pricing-upload/internal/queries"
	"encoding/json"
)

// Client interface defines the method to fetch the patch ID by name.
type Client interface {
	FetchPatchID(name string) (string, error)
}

// client struct holds the GraphQL client for making requests.
type client struct {
	graphQLClient graphql.Client
}

// NewClient initializes and returns a new patches client instance.
func NewClient(graphQLClient graphql.Client) Client {
	return &client{graphQLClient: graphQLClient}
}

// FetchPatchID fetches the patch ID for a given patch name.
func (c *client) FetchPatchID(name string) (string, error) {
	// Define variables for the GraphQL query.
	variables := map[string]interface{}{
		"name": name,
	}

	// Execute the GraphQL query to fetch patches.
	body, err := c.graphQLClient.ExecuteGraphQLQuery(queries.ListPatches, variables)
	if err != nil {
		return "", err
	}

	// Unmarshal the response body into the PatchResponse struct.
	var response models.PatchResponse
	err = json.Unmarshal(body, &response)
	if err != nil {
		return "", err
	}

	// Iterate through the edges to find the patch with the matching name.
	for _, edge := range response.Data.MenuPatches.Edges {
		if edge.Node.Name == name {
			return edge.Node.PatchID, nil
		}
	}

	// Return an empty string if no matching patch is found.
	return "", nil
}
