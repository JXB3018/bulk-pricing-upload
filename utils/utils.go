package utils

import (
	"bulk-pricing-upload/internal/models"
	"encoding/csv"
	"encoding/json"
	"io/ioutil"
	"os"
	"strings"
)

// WriteJSONToFile writes the provided data to a JSON file with the given file name.
func WriteJSONToFile(fileName string, data interface{}) error {
	// Marshal the data into JSON with indentation for readability.
	file, err := json.MarshalIndent(data, "", "  ")
	if err != nil {
		return err
	}

	// Write the JSON data to the specified file.
	err = ioutil.WriteFile(fileName, file, 0644)
	if err != nil {
		return err
	}

	return nil
}

// CreateCSVFromProducts creates a CSV file from a slice of product nodes.
func CreateCSVFromProducts(fileName string, allProducts []models.Node) error {
	// Create a new CSV file.
	file, err := os.Create(fileName)
	if err != nil {
		return err
	}
	defer file.Close()

	// Initialize the CSV writer.
	writer := csv.NewWriter(file)
	defer writer.Flush()

	// Write the CSV header row.
	header := []string{"storeId", "variantId", "price", "DW_PRODUCT", "productdesc", "current_price", "final_price", "SIZE", "CLASS", "BASE", "PROD"}
	if err := writer.Write(header); err != nil {
		return err
	}

	// Iterate through all products to populate the CSV rows.
	for _, product := range allProducts {
		productDesc := product.InternalName
		classLongDesc := strings.Split(product.ProductCode, "|")[0]
		baseLongDesc, prodLongDesc := extractProductCodes(product.ProductCode)

		// Iterate through each variant of the product.
		for _, variant := range product.Variants {
			variantID := variant.VariantID
			sizeLongDesc, baseLongDescVariant, prodLongDescVariant := extractSelectedOptions(variant.SelectedOptions)

			// Override baseLongDesc and prodLongDesc if variant-specific values are present.
			if baseLongDescVariant != "" {
				baseLongDesc = baseLongDescVariant
			}
			if prodLongDescVariant != "" {
				prodLongDesc = prodLongDescVariant
			}

			// Create a CSV record for the variant.
			record := []string{"", variantID, "", "", productDesc, "", "", sizeLongDesc, classLongDesc, baseLongDesc, prodLongDesc}
			if err := writer.Write(record); err != nil {
				return err
			}
		}
	}

	return nil
}

// extractProductCodes extracts BASE and PROD codes from a product code string.
func extractProductCodes(productCode string) (baseLongDesc, prodLongDesc string) {
	// Split the product code by the delimiter "|".
	parts := strings.Split(productCode, "|")
	for _, part := range parts {
		// Check if the part contains "BASE" or "PROD" and assign accordingly.
		if strings.Contains(part, "BASE") {
			baseLongDesc = part
		}
		if strings.Contains(part, "PROD") {
			prodLongDesc = part
		}
	}
	return
}

// extractSelectedOptions extracts SIZE, BASE, and PROD codes from selected options.
func extractSelectedOptions(selectedOptions []models.SelectedOption) (sizeLongDesc, baseLongDesc, prodLongDesc string) {
	// Iterate through each selected option.
	for _, selectedOption := range selectedOptions {
		split := strings.Split(selectedOption.OptionValue.OptionValueCode, "|")
		if len(split) > 1 {
			optionValueCode := split[1]
			// Check if the option value code contains "SIZE", "BASE", or "PROD" and assign accordingly.
			if strings.Contains(optionValueCode, "SIZE") {
				sizeLongDesc = optionValueCode
			}
			if strings.Contains(optionValueCode, "BASE") {
				baseLongDesc = optionValueCode
			}
			if strings.Contains(optionValueCode, "PROD") {
				prodLongDesc = optionValueCode
			}
		}
	}
	return
}
