
# Bulk Pricing Upload Service

The Bulk Pricing Upload Service is a tool for managing bulk pricing operations for products across multiple stores. This service fetches product and store data, processes bulk pricing updates, and generates required files for updating store pricing.

## Table of Contents

- [Prerequisites](#prerequisites)
- [Installation](#installation)
- [Configuration](#configuration)
- [Usage](#usage)
- [Building the Service](#building-the-service)
- [Contributing](#contributing)
- [License](#license)

## Prerequisites

- Go 1.16 or higher
- A Unix-based operating system (Linux or macOS) or Windows
- Environment variables for credentials and URLs

## Installation

1. Clone the repository:

   ```sh
   git clone git@gitlab.com:JXB3018/bulk-pricing-upload.git
   cd bulk-pricing-upload
   ```

2. Install dependencies:

   ```sh
   go mod tidy
   ```

## Configuration

Set up the following environment variables in your shell or in a `.env` file:

- `STK_STG_CREDENTIALS`
- `STK_AUTH_STG_URL`
- `STK_STG_URL`
- `STK_PROD_CREDENTIALS`
- `STK_AUTH_PROD_URL`
- `STK_PROD_URL`

## Usage

To run the service:

```sh
go run cmd/bulk-pricing-upload/main.go
```

You can specify the environment (STG or PROD) using the `-ENV` flag:

```sh
go run cmd/bulk-pricing-upload/main.go -ENV=STG
```

## Building the Service

To build the service for different operating systems:

### For Linux:

```sh
GOOS=linux GOARCH=amd64 go build -o ./dist/bulk-pricing-upload ./cmd/bulk-pricing-upload/main.go
```

### For macOS:

```sh
GOOS=darwin GOARCH=amd64 go build -o ./dist/bulk-pricing-upload ./cmd/bulk-pricing-upload/main.go
```

### For Windows:

```sh
GOOS=windows GOARCH=amd64 go build -o ./dist/bulk-pricing-upload.exe ./cmd/bulk-pricing-upload/main.go
```

The compiled binary will be available in the `./dist` directory.

## Contributing

Contributions are welcome! Please create an issue or submit a pull request to discuss your changes.

## License

This project is licensed under the MIT License. See the [LICENSE](LICENSE) file for details.
