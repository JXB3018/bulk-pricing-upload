package main

import (
	"bulk-pricing-upload/internal/auth"
	"bulk-pricing-upload/internal/bulk"
	"bulk-pricing-upload/internal/graphql"
	"bulk-pricing-upload/internal/menu"
	"bulk-pricing-upload/internal/models"
	"bulk-pricing-upload/internal/patches"
	"bulk-pricing-upload/internal/stores"
	"bulk-pricing-upload/utils"
	"flag"
	"log"
	"os"
)

func main() {
	// Define variables to hold credentials and URLs
	var clientCredentials, authURL, baseURL string

	// Define a flag for environment (default is "STG")
	ENV := flag.String("ENV", "STG", "Current environment")
	flag.Parse()

	// Set credentials and URLs based on the environment
	switch *ENV {
	case "STG":
		clientCredentials = os.Getenv("STK_STG_CREDENTIALS")
		authURL = os.Getenv("STK_AUTH_STG_URL")
		baseURL = os.Getenv("STK_STG_URL")
	case "PROD":
		clientCredentials = os.Getenv("STK_PROD_CREDENTIALS")
		authURL = os.Getenv("STK_AUTH_PROD_URL")
		baseURL = os.Getenv("STK_PROD_URL")
	}

	// Define file paths for product data and CSV templates
	productFile := "prod-products.json"
	csvFilePath := "updated-template.csv"
	outputCSVFile := "base-template.csv"

	// Initialize clients
	menuClient, patchClient, storeClient, err := initializeClients(clientCredentials, authURL, baseURL)
	if err != nil {
		log.Fatalf("Failed to initialize clients: %v", err)
	}

	// Fetch all stores
	allStores, err := fetchAllStores(storeClient)
	if err != nil {
		log.Fatalf("Failed to fetch all stores: %v", err)
	}
	log.Printf("All stores successfully retrieved: %d", len(allStores))
	log.Printf("Stores menu url: %s", allStores[0].CompressedMenuByChannel)

	// Fetch all products
	allProducts, err := fetchAllProducts(menuClient)
	if err != nil {
		log.Fatalf("Failed to fetch all products: %v", err)
	}

	// Write all products to a JSON file
	if err := utils.WriteJSONToFile(productFile, allProducts); err != nil {
		log.Fatalf("Failed to write products to file: %v", err)
	}
	log.Printf("Data successfully written to %s", productFile)

	// Read CSV file and create operations
	storeOperations, err := bulk.ReadCSVAndCreateOperations(csvFilePath)
	if err != nil {
		log.Fatalf("Failed to read CSV and create operations: %v", err)
	}

	// Process store operations
	processStoreOperations(storeOperations, patchClient)

	// Create CSV from products
	if err := utils.CreateCSVFromProducts(outputCSVFile, allProducts); err != nil {
		log.Fatalf("Failed to create CSV from products: %v", err)
	}
	log.Printf("CSV successfully created and written to %s", outputCSVFile)
}

// initializeClients initializes and authorizes the necessary clients for the application
func initializeClients(clientCredentials, authURL, baseURL string) (menu.Client, patches.Client, stores.Client, error) {
	authClient := auth.NewClient(clientCredentials, authURL)
	if err := authClient.Authorize(); err != nil {
		return nil, nil, nil, err
	}

	graphQLClient := graphql.NewClient(authClient, baseURL)
	menuClient := menu.NewClient(graphQLClient)
	patchClient := patches.NewClient(graphQLClient)
	storeClient := stores.NewClient(graphQLClient)

	return menuClient, patchClient, storeClient, nil
}

// fetchAllStores retrieves all stores using the store client
func fetchAllStores(storeClient stores.Client) ([]models.StoreResponse, error) {
	allStores, err := storeClient.FetchAllStores()
	if err != nil {
		return nil, err
	}
	return allStores, nil
}

// fetchAllProducts retrieves all products using the menu client
func fetchAllProducts(menuClient menu.Client) ([]models.Node, error) {
	allProducts, err := menuClient.FetchAllGlobalProducts()
	if err != nil {
		return nil, err
	}
	return allProducts, nil
}

// processStoreOperations processes operations for each store and writes them to JSON files
func processStoreOperations(storeOperations map[string][]models.Operation, patchClient patches.Client) {
	for store, ops := range storeOperations {
		// Fetch patch ID for the store
		patchID, err := patchClient.FetchPatchID(store + "_SetProdPrices")
		if err != nil {
			log.Printf("Error fetching patch ID for store %s: %v", store, err)
			continue
		}

		storeOps := models.StoreOperations{
			Name:          store + "_SetProdPrices",
			PatchID:       patchID,
			SetOperations: ops,
		}

		// TODO: Send Reviews here

		// Write store operations to a JSON file
		if err := utils.WriteJSONToFile(store+"_setOperations.json", storeOps); err != nil {
			log.Printf("Error writing operations to file for store %s: %v", store, err)
			continue
		}

		log.Printf("Operations data successfully written for store: %s", store)
	}
}
